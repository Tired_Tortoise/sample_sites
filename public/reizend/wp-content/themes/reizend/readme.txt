Reizend Theme is Licensed under GPL v3.0
Reizend Theme has been Created by ProThemes.in, Copyright 2013.


Resources:

Underscroes Framework
http://underscores.me
GPL v2

Bootstrap 3.0 Framework
http://getbootstrap.com
Apache 2.0

Font Awesome Icons
http://fontawesome.io
OFL 1.1

Options Framework
http://wptheming.com
GPL v2

All Other CSS/IMAGES/FONTS used in this theme Have been created by me and are under GPL v3.0.