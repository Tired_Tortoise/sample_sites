<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

function optionsframework_options() {

	$options = array();
	$imagepath =  get_template_directory_uri() . '/images/';
	$slider_effect_array = array(
		'random' => __('Random', 'reizend'),
		'sliceDown' => __('Slice Down', 'reizend'),
		'sliceUp' => __('Slice Up', 'reizend'),
		'sliceUpLeft' => __('Slice Up Left', 'reizend'),
		'sliceUpDown' => __('Slice Up Down', 'reizend'),
		'sliceUpDownLeft' => __('Slice Up Down Left', 'reizend'),
		'fold' => __('Fold', 'reizend'),
		'boxRandom' => __('Box Random', 'reizend'),
		'slideInRight' => __('Slide In Right', 'reizend'),
		'slideInLeft' => __('Slide In Left', 'reizend'),
		'boxRain' => __('Box Rain', 'reizend'),
		'boxRainReverse' => __('Box Rain Reverse', 'reizend'),
		'boxRainGrow' => __('Box Rain Grow', 'reizend'),
		'boxRainGrowReverse' => __('Box Rain Grow Reverse', 'reizend'),
		'fade' => __('Fade', 'reizend')
	);	
	$true_false = array(
		'true' => __('true', 'reizend'),
		'false' => __('false', 'reizend')
	);	
	// Get all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	$carousel_count = array(
		'4' => __('4', 'reizend'),
		'5' => __('5', 'reizend'),
		'6' => __('6', 'reizend'),
		'7' => __('7', 'reizend'),
		'8' => __('8', 'reizend'),
		'9' => __('9', 'reizend'),
		'10' => __('10', 'reizend'),
		'11' => __('11', 'reizend'),
		'12' => __('12', 'reizend'),
	);
	
	//Basic Settings
	
	$options[] = array(
		'name' => __('Basic Options', 'reizend'),
		'type' => 'heading');
			
	$options[] = array(
		'name' => __('Site Logo', 'reizend'),
		'desc' => __('Leave Blank to use text Heading.', 'reizend'),
		'id' => 'logo',
		'class' => '',
		'type' => 'upload');
		
		
	$options[] = array(
		'name' => __('Enable Excerpts on Homepage', 'reizend'),
		'desc' => __('By default, the theme shows either the full post or content up till the point where you placed the &lt;!--more--> tag. Check this if you want to you enable Excerpts on Homepage. Excerpts are short summary of your posts.', 'reizend'),
		'id' => 'excerpt1',
		'std' => '0',
		'type' => 'checkbox');	

	$options[] = array(
		'name' => __('Custom Code in Header', 'reizend'),
		'desc' => __('Insert scripts or code before the closing &lt;/head&gt; tag in the document source:', 'reizend'),
		'id' => 'headcode1',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'name' => __('Custom Code in Footer', 'reizend'),
		'desc' => __('Insert scripts or code before the closing &lt;/body&gt; tag in the document source:', 'reizend'),
		'id' => 'footercode1',
		'std' => '',
		'type' => 'textarea');		
	
	$options[] = array(
		'name' => __('Custom CSS', 'reizend'),
		'desc' => __('Some Custom Styling for your site. Place any css codes here instead of the style.css file.', 'reizend'),
		'id' => 'style2',
		'std' => '',
		'type' => 'textarea');
				
	//Social Settings
	
	$options[] = array(
	'name' => __('Social Settings', 'reizend'),
	'type' => 'heading');

	$options[] = array(
		'name' => __('Facebook', 'reizend'),
		'desc' => __('Facebook Profile or Page URL i.e. http://facebook.com/username/ ', 'reizend'),
		'id' => 'facebook',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Twitter', 'reizend'),
		'desc' => __('Twitter Username', 'reizend'),
		'id' => 'twitter',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Google Plus', 'reizend'),
		'desc' => __('Google Plus profile url, including "http://"', 'reizend'),
		'id' => 'google',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Feeburner', 'reizend'),
		'desc' => __('URL for your RSS Feeds', 'reizend'),
		'id' => 'feedburner',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Pinterest', 'reizend'),
		'desc' => __('Your Pinterest Profile URL', 'reizend'),
		'id' => 'pinterest',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Instagram', 'reizend'),
		'desc' => __('Your Instagram Profile URL', 'reizend'),
		'id' => 'instagram',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Linked In', 'reizend'),
		'desc' => __('Your Linked In Profile URL', 'reizend'),
		'id' => 'linkedin',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Youtube', 'reizend'),
		'desc' => __('Your Youtube Channel URL', 'reizend'),
		'id' => 'youtube',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Tumblr', 'reizend'),
		'desc' => __('Your Tumblr Blog URL', 'reizend'),
		'id' => 'tumblr',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Flickr', 'reizend'),
		'desc' => __('Your Flickr Profile URL', 'reizend'),
		'id' => 'flickr',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Dribbble', 'reizend'),
		'desc' => __('Your Dribble Profile URL', 'reizend'),
		'id' => 'dribble',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');			

	return $options;
}