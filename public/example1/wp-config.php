<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'example');

/** MySQL database username */
define('DB_USER', 'forge');

/** MySQL database password */
define('DB_PASSWORD', '9BzSoDcSVbBboi5pgC7W');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hR-s?raj9hyTv|Pvf82-g^J|AM i>VDQhz.+qSdw=B)_/G^P*+S4,k>bgo]IMI_T');
define('SECURE_AUTH_KEY',  'rnl= Q/A)%2((2=~#&Ss{`vb|--!D%6c8mXH{Q%N{{}*7j$K N^0+E8-ZeKdi992');
define('LOGGED_IN_KEY',    '|%F6)>3GH[4XZ2-lbD./M7J>gNElB>6_J;R})r)YR+Y+)A4 /j)J5*E671_YJEqg');
define('NONCE_KEY',        'Xe2ognaOn8q,hVJ-FL,om!{V?o`xg@&c-:AwSQx)et7CS@-JW@=*V89J)j$5p]F#');
define('AUTH_SALT',        'Tf4vROtH3}v1d(+;yrRi#b uV_fKX}j|7V#f5z7hYg]N-A;eq[sH_bq<}C4pHf(`');
define('SECURE_AUTH_SALT', ':K7]W.(nH12bn>6~YmUuNekUk?dBOx?*tzEF0ymf0a](_`7[YP2bido[]?NS+nP8');
define('LOGGED_IN_SALT',   'WhgAkPb?HgD{=QNE7S8V%-2(pv[g_HxE[_|aj&B iDb1D)eeRO_4)5vQ=|Am8w`J');
define('NONCE_SALT',       'woU6hbjFfU3-hOUK2.vsb1[-tMBFq4ncN~mRpuU=s&,5_Q[9ao40d%=3g^!YK&Vg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
